def build_line_graph(vertices):
    line_graph_tmp = []
    tmp_vertices = vertices
    for i in range(1, len(vertices)):
        segment = tmp_vertices[0]
        tmp_vertices.remove(segment)
        for other_segment in tmp_vertices:
            if segment[0] == other_segment[0] or segment[0] == other_segment[1] or segment[1] == other_segment[0] or \
                    segment[1] == other_segment[1]:
                line_graph_tmp.append([segment, other_segment])
    return line_graph_tmp


if __name__ == '__main__':
    # this is the list of all vertices from my Graph that can be found in the git, named GraphSample.png
    vertices_list = [[1, 2], [1, 4], [2, 5], [2, 3], [3, 5], [4, 5]]
    line_graph = build_line_graph(vertices_list)

    for elem in line_graph:
        print(elem)