num_houses_to_look = 452


def get_distance_from_snowplow(house, pos):
    tmp_house = abs(house)
    tmp_pos = abs(pos)
    if house < 0:
        return tmp_house + pos if pos >= 0 else tmp_house - tmp_pos
    else:
        return house - pos if pos >= 0 else house + tmp_pos


def get_most_dense(houses, pos):
    left = list(filter(lambda x: x < 0, houses))
    right = list(filter(lambda x: x >= 0, houses))

    if len(left) == 0:
        return right
    elif len(right) == 0:
        return left

    i = 1
    right_distances = [get_distance_from_snowplow(right[0], pos)]
    while i < len(right) and i < num_houses_to_look:
        right_distances.append(right[i] - right[i - 1])
        i += 1

    i = -2
    left_distances = [get_distance_from_snowplow(abs(left[-1]), pos)]
    while abs(i) <= len(left) and abs(i) <= num_houses_to_look:
        left_distances.append(abs(left[i]) - abs(left[i + 1]))
        i += -1

    left_average = sum(left_distances) / len(left_distances) if len(left_distances) > 0 else 0
    right_average = sum(right_distances) / len(right_distances) if len(right_distances) > 0 else 0

    if left_average == 0 and right_average == 0:
        return []
    elif left_average == 0:
        return right
    elif right_average == 0:
        return left
    else:
        return left if left_average < right_average else right


def parcours(houses):
    tmp_list = list(houses)
    tmp_list.sort()
    res = []
    pos = 0
    total_distance = 0

    while len(tmp_list) > 0:
        side = get_most_dense(tmp_list, pos)
        if side[0] < 0:
            i = -1
            while abs(i) <= len(side) and abs(i) <= num_houses_to_look:
                res.append(side[i])
                total_distance += get_distance_from_snowplow(side[i], pos)
                pos = side[i]
                tmp_list.remove(side[i])
                i += -1
        else:
            i = 0
            while i < len(side) and i < num_houses_to_look:
                res.append(side[i])
                total_distance += get_distance_from_snowplow(side[i], pos)
                pos = side[i]
                tmp_list.remove(side[i])
                i += 1

    return res
